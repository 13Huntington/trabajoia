package net.flood.ocrnn.ui;

import javax.swing.*;
import java.awt.*;

/**
 * @author Cristian
 */
public class MainFrame extends JFrame {
    private static final int FRAME_WIDTH = 700;
    private static final int FRAME_HEIGHT = 1000;
    private QueryPanel2 queryPanel;
    private TrainingPanel trainingPanel;

    public MainFrame() {
        setTitle("Reconocimiento de digitos Dalmasso_Ingaramo_Montero_Torres");
        setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        queryPanel = new QueryPanel2(this, FRAME_WIDTH, FRAME_HEIGHT);
        trainingPanel = new TrainingPanel(this, FRAME_WIDTH, FRAME_HEIGHT);
        setContentPane(queryPanel);
        pack();
        setLocationRelativeTo(null);
    }

    protected void showQueryContent() {
        setContentPane(queryPanel);
        revalidate();
        repaint();
    }

    protected void showTrainingContent() {
        setContentPane(trainingPanel);
        revalidate();
        repaint();
    }

    public void open() {
        SwingUtilities.invokeLater(() -> setVisible(true));
    }

    public static void setSystemLookAndFeel() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
