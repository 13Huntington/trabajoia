package net.flood.ocrnn.ui;

import net.flood.ocrnn.CharacterQueryResult;
import net.flood.ocrnn.Const;
import net.flood.ocrnn.Main;
import net.flood.ocrnn.Task;
import net.flood.ocrnn.util.ImageUtils;
import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class QueryPanel2 extends JPanel implements CharacterCanvas.DrawListener {
	private CharacterCanvas charCanvas;
	private CharacterDrawer charDrawer;
	private JButton clearCanvasButton;
	private JLabel confidenceLabel;
	private JLabel inputTitleLabel;
	private JLabel outputTitleLabel;
	private JLabel resutlsTitleLabel;
	private JButton entrenar;
	private List<PercentageDrawer> percentageDrawerList = new ArrayList<>();
	private JPanel percentageDrawerContainer;
	private MainFrame frame;
	private static int CHAR_WINDOW_SIZE = 240;
	private JScrollPane spConfianza;
	private JTable tblConfianza;

	public QueryPanel2(MainFrame frame ,int width, int height) {
		setBackground(SystemColor.activeCaption);
		this.frame = frame;
		setPreferredSize(new Dimension(500, 750));
		setMaximumSize(new Dimension(500, 750));
		BoxLayout box = new BoxLayout(this, BoxLayout.Y_AXIS);
		setLayout(box);
		JPanel pnlNumeros = new JPanel();
		pnlNumeros.setBackground(SystemColor.activeCaption);
		pnlNumeros.setMaximumSize(new Dimension(500, 250));
		pnlNumeros.setMinimumSize(new Dimension(500, 250));
		pnlNumeros.setLayout(new FlowLayout(FlowLayout.CENTER));
		charCanvas = new CharacterCanvas();
		charCanvas.setPreferredSize(new Dimension(CHAR_WINDOW_SIZE, CHAR_WINDOW_SIZE));
		charCanvas.setMaximumSize(new Dimension(CHAR_WINDOW_SIZE, CHAR_WINDOW_SIZE));
		pnlNumeros.add(charCanvas);
		charDrawer = new CharacterDrawer();
		charDrawer.setPreferredSize(new Dimension(CHAR_WINDOW_SIZE, CHAR_WINDOW_SIZE));
		charDrawer.setMaximumSize(new Dimension(CHAR_WINDOW_SIZE, CHAR_WINDOW_SIZE));
		pnlNumeros.add(charDrawer);
		add(pnlNumeros);
		JPanel pnlCancelar = new JPanel();
		pnlCancelar.setBackground(SystemColor.activeCaption);
		pnlCancelar.setLayout(new FlowLayout(FlowLayout.CENTER));
		
		clearCanvasButton = new JButton("Cancelar");
		clearCanvasButton.setPreferredSize(new Dimension(CHAR_WINDOW_SIZE, 30));
		pnlCancelar.add(clearCanvasButton);
		pnlCancelar.setMaximumSize(new Dimension(500, 40));
		pnlCancelar.setMinimumSize(new Dimension(500, 40));
		add(pnlCancelar);
		JPanel pnlTabla = new JPanel();
		pnlTabla.setLayout(new FlowLayout(FlowLayout.LEFT));
		spConfianza = new JScrollPane();
		tblConfianza = new JTable();
		tblConfianza.setEnabled(false);
		tblConfianza.setRowSelectionAllowed(false);
		
		spConfianza.setViewportView(tblConfianza);
		spConfianza.setMaximumSize(new Dimension(500, 195));
		spConfianza.setMinimumSize(new Dimension(500, 195));
		add(spConfianza);
		cargarModeloTabla();
		confidenceLabel = new JLabel("Confianza");
		inputTitleLabel = new JLabel("Numero de entrada");
		inputTitleLabel.setFont(inputTitleLabel.getFont().deriveFont(Const.TITLE_FONT_SIZE));
		outputTitleLabel = new JLabel("Numero a validar");
		outputTitleLabel.setFont(outputTitleLabel.getFont().deriveFont(Const.TITLE_FONT_SIZE));
		resutlsTitleLabel = new JLabel("CONFIANZA");
		resutlsTitleLabel.setFont(resutlsTitleLabel.getFont().deriveFont(Const.TITLE_FONT_SIZE));
		entrenar = new JButton("Entrenar");
		JPanel pnlEntrenar = new JPanel();
		pnlEntrenar.setBackground(SystemColor.activeCaption);
		pnlEntrenar.setLayout(new FlowLayout(FlowLayout.CENTER));
		pnlEntrenar.add(entrenar);
		pnlEntrenar.setMaximumSize(new Dimension(500, 40));
		pnlEntrenar.setMinimumSize(new Dimension(500, 40));
		add(pnlEntrenar);
		
		initListeners();
		percentageDrawerContainer = new JPanel();
		percentageDrawerContainer.setLayout(new BoxLayout(percentageDrawerContainer, BoxLayout.X_AXIS));
		for (int c = 0; c < 10; c++) {
			PercentageDrawer drawer = new PercentageDrawer(String.valueOf(c));
			drawer.setDestinationValue(0.01f);
			drawer.setPreferredSize(new Dimension(CHAR_WINDOW_SIZE, 21));
			percentageDrawerContainer.add(drawer);
			percentageDrawerList.add(drawer);
			if (c < 9) {
				percentageDrawerContainer.add(Box.createRigidArea(new Dimension(0, 5)));
			}
		}
		repaint();
	}

	   private void cargarModeloTabla() {
		   tblConfianza.setModel(new DefaultTableModel(
				   
				   
		   	new Object[][] {
		   		{0, null},
		   		{1, null},
		   		{2, null},
		   		{3, null},
		   		{4, null},
		   		{5, null},
		   		{6, null},
		   		{7, null},
		   		{8, null},
		   		{9, null},
		   	},
		   	new String[] {
		   		"Numero ingresado", "Confianza"
		   	}
		   ));
		   
		   tblConfianza.getColumnModel().getColumn(0).setResizable(false);
		   tblConfianza.getColumnModel().getColumn(1).setResizable(false);
		   
		   tblConfianza.setDefaultRenderer(Object.class, new DefaultTableCellRenderer()
		   {
		       @Override
		       public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
		       {
		           final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		           Float valor = (Float) table.getModel().getValueAt(row, 1);
		           boolean mayor = true;
		           if(valor != null) {
		           for (int i = 0; i < table.getRowCount(); i++) {
		        	   if (valor < (float)table.getModel().getValueAt(i, 1)) {
						mayor= false;
					}
				}
		           }
		           else {
		        	   mayor = false;
		           }
		        	   c.setBackground(mayor ? Color.LIGHT_GRAY : Color.WHITE);  
		          
		           return c;
		       }
		   });   

	}

	private void initListeners() {
	        clearCanvasButton.addActionListener(e -> charCanvas.clear());
	        entrenar.addActionListener(e -> frame.showTrainingContent());
	        charCanvas.setDrawListener(this);
	    }
	
	   @Override
	    public void onDrawEnd() {
	        Task<CharacterQueryResult> task = Main.query(ImageUtils.deepCopy(charCanvas.getImg()));
	        task.setListener(result -> {
	            try {
	                SwingUtilities.invokeAndWait(() -> {
	                    for(int i = 0; i < percentageDrawerList.size(); i++) {
	                        PercentageDrawer drawer = percentageDrawerList.get(i);
	                        drawer.setDestinationValue((float)result.confidences[i]);
	                    } 
	                    cargarTabla(result);
	                    charDrawer.draw(result.getCharacter());
	                    confidenceLabel.setText("Confidenza: " + String.format("%.2f%%",
	                            100D * result.confidences[Integer.valueOf(String.valueOf(result.getCharacter()))]));
	            });
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	        });
	    }
	   
	   private void cargarTabla(CharacterQueryResult result) {
		  DefaultTableModel modelo = (DefaultTableModel) tblConfianza.getModel();
		   for(int i = 0; i < percentageDrawerList.size(); i++) {
              modelo.setValueAt((float)result.confidences[i], i, 1);
         
           } 
		  
		
	}

	

	    @Override
	    public void onDrawStart() {

	    }
}
